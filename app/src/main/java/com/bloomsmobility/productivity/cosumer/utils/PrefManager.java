package com.bloomsmobility.productivity.cosumer.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Atmaram on 08-06-2017.
 */

public class PrefManager {

    SharedPreferences pref;
    Context _context;
    SharedPreferences.Editor editor;

    int PRIVATE_MODE = 0;
    static final String PREF_USER_NAME = "username";

    private static final String PREF_NAME = "consumer";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public String getUsername() {
        return pref.getString(PREF_USER_NAME, null);
    }

    public void setUsername(String userName) {
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public void removeValue() {
        editor.remove(PREF_USER_NAME);
        editor.commit();
    }

}
