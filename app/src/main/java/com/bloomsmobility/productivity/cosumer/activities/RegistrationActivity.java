package com.bloomsmobility.productivity.cosumer.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.models.ProductData;
import com.bloomsmobility.productivity.cosumer.models.UserDetails;
import com.bloomsmobility.productivity.cosumer.utils.Constants;
import com.bloomsmobility.productivity.cosumer.utils.DialogUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private Context context;

    private Button button_Register;

    private EditText edit_text_name, edit_text_mobile, edit_text_email, edit_text_password, edit_text_reenter_pass, edit_text_address,
            edit_text_pincode;
    public static String email, password;
    private FirebaseDatabase database;
    private com.google.firebase.database.Query queryRef;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressDialog mProgress;
    private ProductData productData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_registration);

        context = RegistrationActivity.this;
        mProgress = new ProgressDialog(context);


        TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tm.getDeviceId(0);
        }

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        edit_text_name = (EditText) findViewById(R.id.editText_userName);
        edit_text_mobile = (EditText) findViewById(R.id.editText_phoneNo);
        edit_text_email = (EditText) findViewById(R.id.editText_email);
        edit_text_address = (EditText) findViewById(R.id.editText_door);
        edit_text_pincode = (EditText) findViewById(R.id.editText_pincode);
        edit_text_password = (EditText) findViewById(R.id.editText_password);

        productData = ProductData.getInstance();

        password = edit_text_password.getText().toString();

        button_Register = (Button) findViewById(R.id.button_register);

        button_Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_text_name.getText().toString().trim().isEmpty()) {
                    edit_text_name.setError(getString(R.string.name_is_required));
                    edit_text_name.requestFocus();
                    showError(edit_text_name);
                } else if (edit_text_mobile.getText().toString().trim().length() < 0) {
                    edit_text_mobile.setError(getString(R.string.mobile_number));
                    edit_text_mobile.requestFocus();
                    showError(edit_text_mobile);
                } else if (!isValidEmail(edit_text_email.getText().toString().trim())) {
                    edit_text_email.setError(getString(R.string.invalid_email));
                    edit_text_email.requestFocus();
                    showError(edit_text_email);
                } else if (edit_text_address.getText().toString().trim().isEmpty()) {
                    edit_text_address.setError(getString(R.string.address_is_required));
                    edit_text_address.requestFocus();
                    showError(edit_text_address);
                } else if (edit_text_pincode.getText().toString().trim().isEmpty()) {
                    edit_text_pincode.setError(getString(R.string.pincode_required));
                    edit_text_pincode.requestFocus();
                    showError(edit_text_pincode);
                } else if (edit_text_password.getText().toString().trim().isEmpty()) {
                    edit_text_password.setError(getString(R.string.password_is_required));
                    edit_text_password.requestFocus();
                    showError(edit_text_password);
                } else if (edit_text_password.getText().toString().length() < 0) {
                    edit_text_password.setError(getString(R.string.password_length));
                }
                else {
                    usersTable();
                }
            }
        });
    }

    private void usersTable() {

        DialogUtil.displayProgress(RegistrationActivity.this);
        email = edit_text_email.getText().toString();
        password = edit_text_password.getText().toString();

        DatabaseReference myRef = database.getReference(Constants.TABLE_USER);
        Map<String, String> post1 = new HashMap<>();

        post1.put(Constants.COLUMN_USER_NAME, edit_text_name.getText().toString());
        post1.put(Constants.COLUMN_USER_CONTACT, edit_text_mobile.getText().toString());
        post1.put(Constants.COLUMN_USER_EMAIL_ID, edit_text_email.getText().toString());
        post1.put(Constants.COLUMN_USER_ADDRESS, edit_text_address.getText().toString());
        post1.put(Constants.COLUMN_USER_PINCODE, edit_text_pincode.getText().toString());
        post1.put(Constants.COLUMN_USER_PASSWORD, edit_text_password.getText().toString());
        myRef.push().setValue(post1);

        Toast.makeText(context, getString(R.string.registered_success), Toast.LENGTH_SHORT).show();

        edit_text_name.setText("");
        edit_text_mobile.setText("");
        edit_text_email.setText("");
        edit_text_password.setText("");
        edit_text_pincode.setText("");
        edit_text_address.setText("");

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("Account ", getString(R.string.create_account) + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            DialogUtil.stopProgressDisplay();
                        } else {
                            Intent intent = new Intent(RegistrationActivity.this, DemoLoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        DialogUtil.stopProgressDisplay();
                    }
                });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                    .matches();
        }
    }

    private void showError(EditText editText) {
        Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);
        editText.startAnimation(shake);
    }
}
