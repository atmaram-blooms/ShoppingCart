package com.bloomsmobility.productivity.cosumer.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.bloomsmobility.productivity.cosumer.Cart;
import com.bloomsmobility.productivity.cosumer.CartHelper;
import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.Saleable;
import com.bloomsmobility.productivity.cosumer.adapters.ProductAdapter;
import com.bloomsmobility.productivity.cosumer.listener.OnFragmentListItemSelectListener;
import com.bloomsmobility.productivity.cosumer.models.CartItems;
import com.bloomsmobility.productivity.cosumer.models.ProductData;
import com.bloomsmobility.productivity.cosumer.models.Products;
import com.bloomsmobility.productivity.cosumer.utils.Constants;
import com.bloomsmobility.productivity.cosumer.utils.PrefManager;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.bloomsmobility.productivity.cosumer.activities.MainActivity.text_counter;

/**
 * Created by Atmaram on 05-06-2017.
 */

public class ShoppingCart extends BaseFragment implements OnFragmentListItemSelectListener {

    static private ProductAdapter mProductAdapter;
    View rootView;

    static ListView listViewCatalog;
    Button removeButton, checkout;
    FirebaseDatabase database;
    PrefManager prefManager;
    static Cart cart;
    CartItems selectedProduct;
    static int count;
    CartItems cartItem;
    TextView empty_view;
    static List<CartItems> cartItems;

    public ShoppingCart() {
    }

    static ShoppingCart fragment;

    public static ShoppingCart newInstance() {
        if (fragment == null) {
            fragment = new ShoppingCart();
        }

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_cart, container, false);
        listViewCatalog = (ListView) rootView.findViewById(R.id.ListViewCatalog);
        removeButton = (Button) rootView.findViewById(R.id.ButtonRemoveFromCart);
        checkout = (Button) rootView.findViewById(R.id.Button02);
        empty_view = (TextView) rootView.findViewById(R.id.empty_view);
        database = FirebaseDatabase.getInstance();

        prefManager = new PrefManager(getActivity());

        cart = CartHelper.getCart();
        displayProgress();

        if (getCartItems(cart).size() == 0) {
            checkout.setEnabled(false);
            empty_view.setVisibility(View.VISIBLE);
            listViewCatalog.setVisibility(View.GONE);
        } else {
            checkout.setEnabled(true);
            empty_view.setVisibility(View.GONE);
            listViewCatalog.setVisibility(View.VISIBLE);
        }

        for (int i = 0; i < getCartItems(cart).size(); i++) {
            getCartItems(cart).get(i).selected = false;
        }

        mProductAdapter = new ProductAdapter(getCartItems(cart), getActivity().getLayoutInflater(), true);
        mProductAdapter.notifyDataSetInvalidated();
        listViewCatalog.setAdapter(mProductAdapter);
        mProductAdapter.setListener(this);

        listViewCatalog.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedProduct = getCartItems(cart).get(position);
                if (selectedProduct.selected == true)
                    selectedProduct.selected = false;
                else
                    selectedProduct.selected = true;

                mProductAdapter.notifyDataSetInvalidated();

            }
        });
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductData.getInstance().setQty(String.valueOf(getCartItems(cart).size()));
                ProductData.getInstance().setPrice(String.valueOf(cart.getTotalPrice()));
                mListener.onFragmentInteraction(Constants.FRAGMENT_CHECKOUT, null);
            }
        });
        return rootView;
    }

    public static void clearList() {
        cart.clear();
        text_counter.setText(String.valueOf(0));
    }

    @Override
    public void onListItemSelected(final int itemId, Object data) {
//        tvTotalPrice.setText(Constant.CURRENCY+String.valueOf(cart.getTotalPrice().setScale(2, BigDecimal.ROUND_HALF_UP)));
    }

    @Override
    public void onListItemLongClicked(final int itemId, Object data, View view) {
        new AlertDialog.Builder(this.getActivity())
                .setTitle(getResources().getString(R.string.delete_item))
                .setMessage(getResources().getString(R.string.delete_item_message))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        List<CartItems> cartItems = getCartItems(cart);
                        cart.remove(cartItems.get(itemId).getProduct());
                        cartItems.remove(itemId);
                        mProductAdapter.updateCartItems(cartItems);
                        mProductAdapter.notifyDataSetInvalidated();
//                        tvTotalPrice.setText(Constant.CURRENCY+String.valueOf(cart.getTotalPrice().setScale(2, BigDecimal.ROUND_HALF_UP)));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), null)
                .show();
    }

    private List<CartItems> getCartItems(Cart cart) {
        cartItems = new ArrayList<CartItems>();

        Map<Saleable, Integer> itemMap = cart.getItemWithQuantity();

        for (Map.Entry<Saleable, Integer> entry : itemMap.entrySet()) {
            cartItem = new CartItems();
            cartItem.setProduct((Products) entry.getKey());
            cartItem.setCqty(entry.getValue().toString());
            cartItems.add(cartItem);
        }
        dismissProgress();

        count = Integer.parseInt(String.valueOf(cartItems.size()));
        text_counter.setText(String.valueOf(count));
        return cartItems;
    }

    ProgressDialog progressDialog;

    private void displayProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading));
        }
        progressDialog.show();
    }

    private void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

}
