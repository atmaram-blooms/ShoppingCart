package com.bloomsmobility.productivity.cosumer.models;

/**
 * Created by Atmaram on 01-06-2017.
 */

public class UserDetails {

    String name;
    String contact;
    String email;
    String address;
    String pincode;
    String password;

    public UserDetails() {
    }

    public UserDetails(String name, String contact, String email, String address, String pincode, String password) {
        this.name = name;
        this.contact = contact;
        this.email = email;
        this.address = address;
        this.pincode = pincode;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
