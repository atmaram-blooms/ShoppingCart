package com.bloomsmobility.productivity.cosumer.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.fragments.CheckoutFragment;
import com.bloomsmobility.productivity.cosumer.fragments.ProductDetails;
import com.bloomsmobility.productivity.cosumer.fragments.ProductFragment;
import com.bloomsmobility.productivity.cosumer.fragments.ShoppingCart;
import com.bloomsmobility.productivity.cosumer.listener.OnFragmentInteractionListener;
import com.bloomsmobility.productivity.cosumer.utils.Constants;
import com.bloomsmobility.productivity.cosumer.utils.PrefManager;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    private static final String TAG = " MainActivity";
    private FragmentManager mFragmentManager;
    private String mFragmentTag;
    private int mCurrentFragment;
    public static TextView text_counter;
    private Toolbar toolbar;
    private ImageView image_cart, cart_item, image_logout;
    int currentFragment;
    int count;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefManager = new PrefManager(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        text_counter = (TextView) findViewById(R.id.text_cart_item);
        image_cart = (ImageView) findViewById(R.id.image_cart);
        cart_item = (ImageView) findViewById(R.id.cart_item);
        image_logout = (ImageView) findViewById(R.id.image_logout);
        image_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutApp();
            }
        });
        image_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFragmentInteraction(Constants.FRAGMENT_CART, null);
            }
        });

        cart_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoHomePage();
//                onFragmentInteraction(Constants.FRAGMENT_PRODUCT, null);
            }
        });

        onFragmentInteraction(Constants.FRAGMENT_PRODUCT, null);
    }

    public void logoutApp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.logout_message));

        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(MainActivity.this, DemoLoginActivity.class);
                startActivity(intent);
                finish();

                FirebaseAuth.getInstance().signOut();
                prefManager.removeValue();
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onFragmentInteraction(int fragmentId, Object data) {
        mFragmentManager = getSupportFragmentManager();
        mCurrentFragment = fragmentId;
        mFragmentTag = String.valueOf(fragmentId);

        switch (fragmentId) {

            case Constants.FRAGMENT_PRODUCT:
                mFragmentManager
                        .beginTransaction()
                        .addToBackStack(mFragmentTag)
                        .replace(R.id.fragment_main, ProductFragment.newInstance(),
                                mFragmentTag).commit();
                break;
            case Constants.FRAGMENT_PRODUCT_DETAILS:
                mFragmentManager
                        .beginTransaction()
                        .addToBackStack(mFragmentTag)
                        .replace(R.id.fragment_main, ProductDetails.newInstance((String) data),
                                mFragmentTag).commit();
                break;
            case Constants.FRAGMENT_CART:
                mFragmentManager
                        .beginTransaction()
                        .addToBackStack(mFragmentTag)
                        .replace(R.id.fragment_main, ShoppingCart.newInstance(),
                                mFragmentTag).commit();
                break;
            case Constants.FRAGMENT_CHECKOUT:
                mFragmentManager
                        .beginTransaction()
                        .addToBackStack(mFragmentTag)
                        .replace(R.id.fragment_main, CheckoutFragment.newInstance(),
                                mFragmentTag).commit();
                break;
        }
    }

    public void gotoHomePage() {

        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        onFragmentInteraction(Constants.FRAGMENT_PRODUCT, false);

    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count <= 1) {
            closeApp();
        } else if (mCurrentFragment == Constants.FRAGMENT_CHECKOUT || mCurrentFragment == Constants.FRAGMENT_CART
                || mCurrentFragment == Constants.FRAGMENT_PRODUCT) {
            gotoHomePage();
        } else {
            super.onBackPressed();
        }
    }

    public void closeApp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.quit_message));

        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                finish();

            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}