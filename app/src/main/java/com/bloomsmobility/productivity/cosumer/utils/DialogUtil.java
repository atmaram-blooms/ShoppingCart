package com.bloomsmobility.productivity.cosumer.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;


/**
 * Custom class used to display a Dialog box or a ProgressDialog. This class contain various functions to show Dialog with
 * different layouts. eg- Dialog with OK button, OK & Cancel button etc. The class also provides ability
 * to add listeners to the custom Dialogs so that we can perform custom operations depending on the user
 * interaction.
 */
public class DialogUtil {
    /**
     * Using Static instances of AlertDialog & ProgressDialog so there's a singleton instance.
     */
    static AlertDialog currentDialog;
    static ProgressDialog m_cProgressBar;

    /**
     * Function to Display a AlertDialogBox with a custom title, message, single button and a listener.
     * @param pContext Activity Instance of the activity which called this function
     * @param title String containing the title of the Dialog box
     * @param msg String containing the message to be displayed on Dialog Box
     * @param buttonText String containing the button text
     * @param listner View.OnClickListener listener which is set on the button
     */
    public static void displayMesage(Activity pContext, String title,
                                     String msg, String buttonText,
                                     View.OnClickListener listner) {
        if (pContext.isFinishing())
            return;
        if (currentDialog!=null) {
            //get the Context object that was used to great the dialog
            Context context = ((ContextWrapper) currentDialog.getContext()).getBaseContext();
            //if the Context used here was an activity AND it hasn't been finished or destroyed
            //then dismiss it
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing())
                    currentDialog.dismiss();
            } else //if the Context used wasnt an Activity, then dismiss it too
                currentDialog.dismiss();
        }

        stopProgressDisplay();
//        AlertDialog.Builder alert = new AlertDialog.Builder(pContext);
//        LinearLayout dialogLayout = (LinearLayout) LayoutInflater.from(pContext).inflate(
//                R.layout.alert_dialog_view, null);
//        TextView titleView = (TextView) dialogLayout.findViewById(R.id.dialog_title);
//        if (title != null) {
//            titleView.setText(title);
//        } else {
//            dialogLayout.findViewById(R.id.dialog_title_layout).setVisibility(View.GONE);
//        }
//
//        dialogLayout.findViewById(R.id.dialog_close).setVisibility(View.GONE);
//        TextView titleText = (TextView) dialogLayout.findViewById(R.id.dialog_text);
//        titleText.setText(msg);
//
//        Button btn = (Button) dialogLayout.findViewById(R.id.btn_neutral);
//        btn.setText(buttonText);
//        btn.setOnClickListener(listner);
//        alert.setView(dialogLayout);
//        alert.setCancelable(false);
//        currentDialog = alert.create();
//        currentDialog.show();
    }

    /**
     * Function to Display a AlertDialogBox with a custom title, message, single button and a listener.
     * @param pContext Activity Instance of the activity which called this function
     * @param title String containing the title of the Dialog box
     * @param msg String containing the message to be displayed on Dialog Box
     * @param buttonText String containing the button text
     * @param listner View.OnClickListener listener which is set on the button
     */
    public static void displayMesageUnCancelable(final Activity pContext, String title,
                                                 String msg, String buttonText,
                                                 View.OnClickListener listner) {
        stopProgressDisplay();
        if(pContext == null) return;
        if (pContext.isFinishing())
            return;
        if (currentDialog!=null) {
            //get the Context object that was used to great the dialog
            Context context = ((ContextWrapper) currentDialog.getContext()).getBaseContext();
            //if the Context used here was an activity AND it hasn't been finished or destroyed
            //then dismiss it
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing())
                    currentDialog.dismiss();
            } else //if the Context used wasnt an Activity, then dismiss it too
                currentDialog.dismiss();
        }


//        AlertDialog.Builder alert = new AlertDialog.Builder(pContext);
//        LinearLayout dialogLayout = (LinearLayout) LayoutInflater.from(pContext).inflate(
//                R.layout.alert_dialog_view, null);
//        TextView titleView = (TextView) dialogLayout.findViewById(R.id.dialog_title);
//        if (title != null) {
//            titleView.setText(title);
//        } else {
//            dialogLayout.findViewById(R.id.dialog_title_layout).setVisibility(View.GONE);
//        }
//
//        dialogLayout.findViewById(R.id.dialog_close).setVisibility(View.GONE);
//        TextView titleText = (TextView) dialogLayout.findViewById(R.id.dialog_text);
//        titleText.setText(msg);
//
//        Button btn = (Button) dialogLayout.findViewById(R.id.btn_neutral);
//        btn.setText(buttonText);
//        btn.setOnClickListener(listner);
//        alert.setView(dialogLayout);
//        currentDialog = alert.create();
//        currentDialog.show();
//        currentDialog.setCanceledOnTouchOutside(false);
//        currentDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialogInterface) {
//                pContext.finish();
//            }
//        });
    }

    /**
     * Function to show a retry-cancel Dialog screen.
     * @param pContext Activity Instance of the activity which called this function
     * @param title String containing the title of the Dialog box
     * @param msg String containing the message to be displayed on Dialog Box
     * @param connect Connect class instance. It is used to send retry request to the server.
     * @param lastRequest LastRequest class instance data related to the failed request sent to server.
     *                    This object is used to send a fresh request to the server.
     */
//    public static void displayRetryMesage(final Activity pContext, String title,
//                                          String msg,
//                                          final Connect connect, final LastRequest lastRequest) {
//        if (pContext.isFinishing())
//            return;
//        if (currentDialog!=null) {
//            //get the Context object that was used to great the dialog
//            Context context = ((ContextWrapper) currentDialog.getContext()).getBaseContext();
//            //if the Context used here was an activity AND it hasn't been finished or destroyed
//            //then dismiss it
//            if (context instanceof Activity) {
//                if (!((Activity) context).isFinishing())
//                    if (currentDialog.isShowing())
//                    currentDialog.dismiss();
//            } else //if the Context used wasnt an Activity, then dismiss it too
//                currentDialog.dismiss();
//        }
//
//        stopProgressDisplay();
//        AlertDialog.Builder alert = new AlertDialog.Builder(pContext);
//        LinearLayout dialogLayout = (LinearLayout) LayoutInflater.from(pContext).inflate(
//                R.layout.alert_dialog_view, null);
//        TextView titleView = (TextView) dialogLayout.findViewById(R.id.dialog_title);
//        if (title != null) {
//            titleView.setText(title);
//        } else {
//            dialogLayout.findViewById(R.id.dialog_title_layout).setVisibility(View.GONE);
//        }
//
//        dialogLayout.findViewById(R.id.dialog_close).setVisibility(View.GONE);
//        TextView titleText = (TextView) dialogLayout.findViewById(R.id.dialog_text);
//        titleText.setText(msg);
//
//        dialogLayout.findViewById(R.id.btn_neutral).setVisibility(View.GONE);
//        dialogLayout.findViewById(R.id.two_btn_layout).setVisibility(View.VISIBLE);
//
//        Button btn = (Button) dialogLayout.findViewById(R.id.btn_positive);
//        btn.setText("Retry");
//        Button btn1 = (Button) dialogLayout.findViewById(R.id.btn_negetive);
//        btn1.setText("Cancel");
//        btn1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismissDialog();
//                pContext.finish();
//            }
//        });
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismissDialog();
//                DialogUtil.displayProgress(pContext);
//                //connect.resendRequest(lastRequest);
//            }
//        });
//        alert.setView(dialogLayout);
//        alert.setCancelable(false);
//        currentDialog = alert.create();
//        currentDialog.show();
//    }

    /**
     * Function to show a Dialog with two custom text buttons.
     * @param pContext Activity Instance of the activity which called this function
     * @param title String containing the title of the Dialog box
     * @param msg String containing the message to be displayed on Dialog Box
     * @param button1Text String containing the text of the button 1
     * @param listner1 OnClickListener for button 1
     * @param button2Text String containing the text of the button 2
     * @param listner2 OnClickListener for button 2
     */
    public static void displayMesage(Activity pContext, String title,
                                     String msg, String button1Text,

                                     View.OnClickListener listner1, String button2Text, View.OnClickListener listner2) {
        stopProgressDisplay();
        if (pContext == null)
            return;

        if (pContext.isFinishing())
            return;

        if (currentDialog!=null) {
            currentDialog.setCancelable(false);
            //get the Context object that was used to great the dialog
            Context context = ((ContextWrapper) currentDialog.getContext()).getBaseContext();
            //if the Context used here was an activity AND it hasn't been finished or destroyed
            //then dismiss it
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing())
                    if (currentDialog.isShowing())
                        currentDialog.dismiss();
            } else //if the Context used wasnt an Activity, then dismiss it too
                currentDialog.dismiss();
        }


//        AlertDialog.Builder alert = new AlertDialog.Builder(pContext);
//        LinearLayout dialogLayout = (LinearLayout) LayoutInflater.from(pContext).inflate(
//                R.layout.alert_dialog_view, null);
//        TextView titleView = (TextView) dialogLayout.findViewById(R.id.dialog_title);
//        if (title != null) {
//            titleView.setText(title);
//        } else {
//            dialogLayout.findViewById(R.id.dialog_title_layout).setVisibility(View.GONE);
//        }
//        dialogLayout.findViewById(R.id.dialog_close).setVisibility(View.GONE);
//        TextView titleText = (TextView) dialogLayout.findViewById(R.id.dialog_text);
//        titleText.setText(msg);
//
//        dialogLayout.findViewById(R.id.btn_neutral).setVisibility(View.GONE);
//        dialogLayout.findViewById(R.id.two_btn_layout).setVisibility(View.VISIBLE);
//
//        Button btn1 = (Button) dialogLayout.findViewById(R.id.btn_negetive);
//        btn1.setText(button1Text);
//        btn1.setOnClickListener(listner1);
//
//        Button btn2 = (Button) dialogLayout.findViewById(R.id.btn_positive);
//        btn2.setText(button2Text);
//        btn2.setOnClickListener(listner2);
//
//        alert.setView(dialogLayout);
//        alert.setCancelable(false);
//        currentDialog = alert.create();
//        currentDialog.setCancelable(false);
//        currentDialog.show();
    }

    /**
     * Function to show a custom AlertDialog with a single 'OK' button
     * @param pContext Activity Instance of the activity which called this function
     * @param title String containing the title of the Dialog box
     * @param message String containing the message of the Dialog box
     */
    public static void displayFlashMesage(Activity pContext, String title,
                                          String message) {
        displayMesageWithOk(pContext, title, message);
    }






    public static void displayMesage(Activity pContext, String title,
                                     String msg, String buttonText,
                                     DialogInterface.OnClickListener listner) {
        if (pContext.isFinishing())
            return;
        if (currentDialog!=null) {
            //get the Context object that was used to great the dialog
            Context context = ((ContextWrapper) currentDialog.getContext()).getBaseContext();
            //if the Context used here was an activity AND it hasn't been finished or destroyed
            //then dismiss it
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing())
                    currentDialog.dismiss();
            } else //if the Context used wasnt an Activity, then dismiss it too
                currentDialog.dismiss();
        }

        stopProgressDisplay();
        AlertDialog.Builder alert = new AlertDialog.Builder(pContext);
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setPositiveButton(buttonText, listner);
//        LinearLayout dialogLayout = (LinearLayout) LayoutInflater.from(pContext).inflate(
//                R.layout.alert_dialog_view, null);
//        TextView titleView = (TextView) dialogLayout.findViewById(R.id.dialog_title);
//        if (title != null) {
//            titleView.setText(title);
//        } else {
//            dialogLayout.findViewById(R.id.dialog_title_layout).setVisibility(View.GONE);
//        }
//
//        dialogLayout.findViewById(R.id.dialog_close).setVisibility(View.GONE);
//        TextView titleText = (TextView) dialogLayout.findViewById(R.id.dialog_text);
//        titleText.setText(msg);
//
//        Button btn = (Button) dialogLayout.findViewById(R.id.btn_neutral);
//        btn.setText(buttonText);
//        btn.setOnClickListener(listner);
//        alert.setView(dialogLayout);
        currentDialog = alert.create();
        currentDialog.show();
    }



    /**
     * Function to show a custom AlertDialog with 'OK' button which closes the AlertDialog when clicked.
     * @param pContext Activity Instance of the activity which called this function
     * @param title String containing the title of the Dialog box
     * @param message String containing the message of the Dialog box
     */
    public static void displayMesageWithOk(Activity pContext, String title,
                                           String message) {

        stopProgressDisplay();
        if (pContext == null)
            return;
        if (pContext.isFinishing())
            return;

        if (TextUtils.isEmpty(message))
            return;
        if (currentDialog!=null) {
            //get the Context object that was used to great the dialog
            Context context = ((ContextWrapper) currentDialog.getContext()).getBaseContext();
            //if the Context used here was an activity AND it hasn't been finished or destroyed
            //then dismiss it
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing())
                    if (currentDialog.isShowing())
                        currentDialog.dismiss();
            } else //if the Context used wasnt an Activity, then dismiss it too
                if (currentDialog.isShowing())
                    currentDialog.dismiss();
        }


//        AlertDialog.Builder alert = new AlertDialog.Builder(pContext);
//        LinearLayout dialogLayout = (LinearLayout) LayoutInflater.from(pContext).inflate(
//                R.layout.alert_dialog_view, null);
//        TextView titleView = (TextView) dialogLayout.findViewById(R.id.dialog_title);
//        if (title != null) {
//            titleView.setText(title);
//        } else {
//            dialogLayout.findViewById(R.id.dialog_title_layout).setVisibility(View.GONE);
//        }
//
//        TextView titleText = (TextView) dialogLayout.findViewById(R.id.dialog_text);
//        titleText.setText(message);
//        dialogLayout.findViewById(R.id.dialog_close).setVisibility(View.GONE);
//
//        Button btn = (Button) dialogLayout.findViewById(R.id.btn_neutral);
//        btn.setText("Ok");
//        btn.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                currentDialog.dismiss();
//            }
//        });
//        alert.setView(dialogLayout);
//        currentDialog = alert.create();
//        currentDialog.show();

    }

    /**
     * Function to show a 'Please wait' ProgressDialog
     * @param pContext Activity Instance of the activity which called this function
     */
    public static void displayProgress(Activity pContext) {
        displayProgress(pContext, "Please wait..");
    }

    public static void displayProgress(Activity pContext, String message) {

        if (null == m_cProgressBar) {
            // IsShowoingOneDialog = true;
            if(pContext == null) return;
            if (pContext.isFinishing())
                return;
            m_cProgressBar = new ProgressDialog(pContext);
            m_cProgressBar.setCancelable(false);
            m_cProgressBar.setMessage(message);
            m_cProgressBar.show();

            // Log.d("", "displayProgress shown");
        }else{
            m_cProgressBar.setMessage(message);
        }
    }

    /**
     * Function to dismiss the ProgressDialog
     */
    public static void stopProgressDisplay() {
        if (null != m_cProgressBar) {
            try {
                // IsShowoingOneDialog = false;
                m_cProgressBar.dismiss();
            } catch (Exception e) {

            }
        }
        m_cProgressBar = null;
    }

    /**
     * Function to Dismiss the AlertDialog
     */
    public static void dismissDialog() {
        // TODO Auto-generated method stub
        if (currentDialog!=null) {
            //get the Context object that was used to great the dialog
            Context context = ((ContextWrapper) currentDialog.getContext()).getBaseContext();
            //if the Context used here was an activity AND it hasn't been finished or destroyed
            //then dismiss it
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing())
                    if (currentDialog.isShowing())
                        currentDialog.dismiss();
            } else //if the Context used wasnt an Activity, then dismiss it too
                currentDialog.dismiss();
        }
    }

    /**
     * Function to show a AlertDialog with phone number list. On selecting any number, an intent for
     * telephone call will be fired.
     * @param activity Activity Instance of the activity which called this function
     * @param phone String containing the phone numbers separated by a comma (,)
     * @param retailerName String containing the name of the retailer
     */
    public static void showCallDialog(final Activity activity, String phone, final String retailerName) {
        final String[] phoneNumbers = phone.split(",");

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

//        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(activity).inflate(R.layout.dialog_title, null);
//        TextView title = (TextView) layout.findViewById(R.id.dialog_title);
//        title.setText("Call");
//        TextView icon = (TextView) layout.findViewById(R.id.dialog_icon);
//        icon.setText(R.string.fa_phone);
//        TextView close = (TextView) layout.findViewById(R.id.dialog_close);
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                currentDialog.dismiss();
//            }
//        });
//
//
//        builder.setItems(phoneNumbers, new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
//                        + phoneNumbers[which]));
//                //activity.startActivity(intent);
////                GATracker.getInstance().sendEvent(activity.getApplication(),
////                        GATracker.EVENT_CALL, GATracker.ACTION_CLICK,
////                        "Opened From " + activity.getClass().getSimpleName() + " for calling retailer=" + retailerName);
//            }
//        });
//
//        builder.setCustomTitle(layout);
//
//        currentDialog = builder.create();
//        currentDialog.show();
    }

    public static boolean isProgressDialogShowing(){
        if (m_cProgressBar!=null)
            return m_cProgressBar.isShowing();
        return false;
    }


}

