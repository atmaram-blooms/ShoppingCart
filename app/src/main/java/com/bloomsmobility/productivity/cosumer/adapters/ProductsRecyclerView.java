package com.bloomsmobility.productivity.cosumer.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.listener.OnFragmentListItemSelectListener;
import com.bloomsmobility.productivity.cosumer.models.Products;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Atmaram on 01-06-2017.
 */

public class ProductsRecyclerView extends RecyclerView.Adapter<ProductsRecyclerView.ViewHolder> {

    Activity context;
    OnFragmentListItemSelectListener mListener;
    List<Products> deviceDetails;

    public ProductsRecyclerView(Activity activity, List<Products> deviceDetailsList) {
        this.deviceDetails = deviceDetailsList;
        this.context = activity;
    }

    public void setListener(OnFragmentListItemSelectListener mListener) {
        this.mListener = mListener;
    }


    public void updateCartItems(List<Products> cartItems) {
        this.deviceDetails = cartItems;
        notifyDataSetChanged();
    }

    @Override
    public ProductsRecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(context).inflate(R.layout.products_details_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(row);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Products products = deviceDetails.get(position);

        holder.text_email.setText(products.getPname());
        holder.text_imei.setText(String.valueOf(products.getPrice()));
        holder.text_model.setText(products.getQuantity());

        Picasso.with(context)
                .load(products.getUrl())
                .into(holder.image_icon);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onListItemSelected(position, products);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return deviceDetails.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View mView;
        private TextView text_email, text_imei, text_model, text_name;
        private ImageView image_icon;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            text_email = (TextView) itemView.findViewById(R.id.text_email);
            text_imei = (TextView) itemView.findViewById(R.id.text_imei);
            text_model = (TextView) itemView.findViewById(R.id.text_model);
//            text_name = (TextView) itemView.findViewById(R.id.text_name);
            image_icon = (ImageView) itemView.findViewById(R.id.image_icon);

        }

        @Override
        public void onClick(View view) {
            mListener.onListItemSelected(view.getId(), deviceDetails.get(getLayoutPosition()));
        }
    }
}

