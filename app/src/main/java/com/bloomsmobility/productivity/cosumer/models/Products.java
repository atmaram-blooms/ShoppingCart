package com.bloomsmobility.productivity.cosumer.models;

import com.bloomsmobility.productivity.cosumer.Saleable;

import java.math.BigDecimal;

/**
 * Created by Atmaram on 01-06-2017.
 */

public class Products implements Saleable {

    String pname;
    double price;
    String quantity;
    String description;
    int units;
    String url;
    public boolean selected;

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String getName() {
        return null;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
