package com.bloomsmobility.productivity.cosumer.models;

/**
 * Created by Atmaram on 06-06-2017.
 */

public class CartItems {

    private Products product;

    String cpname;
    String cqty;
    String cdesc;
    String cprice;
    String caddress;
    String ccontact;
    String cpincode;
    String curl;
    String cid;
    public boolean selected;

    public CartItems() {
    }

    public CartItems(String cpname, String cqty, String cdesc, String cprice, String caddress, String ccontact, String cpincode, String curl) {
        this.cpname = cpname;
        this.cqty = cqty;
        this.cdesc = cdesc;
        this.cprice = cprice;
        this.caddress = caddress;
        this.ccontact = ccontact;
        this.cpincode = cpincode;
        this.curl = curl;
    }

    public String getCpname() {
        return cpname;
    }

    public void setCpname(String cpname) {
        this.cpname = cpname;
    }

    public String getCqty() {
        return cqty;
    }

    public void setCqty(String cqty) {
        this.cqty = cqty;
    }

    public String getCdesc() {
        return cdesc;
    }

    public void setCdesc(String cdesc) {
        this.cdesc = cdesc;
    }

    public String getCprice() {
        return cprice;
    }

    public void setCprice(String cprice) {
        this.cprice = cprice;
    }

    public String getCaddress() {
        return caddress;
    }

    public void setCaddress(String caddress) {
        this.caddress = caddress;
    }

    public String getCcontact() {
        return ccontact;
    }

    public void setCcontact(String ccontact) {
        this.ccontact = ccontact;
    }

    public String getCpincode() {
        return cpincode;
    }

    public void setCpincode(String cpincode) {
        this.cpincode = cpincode;
    }

    public String getCurl() {
        return curl;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void setCurl(String curl) {
        this.curl = curl;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }
}
