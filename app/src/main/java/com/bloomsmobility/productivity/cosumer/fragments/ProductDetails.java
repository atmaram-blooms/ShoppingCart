package com.bloomsmobility.productivity.cosumer.fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bloomsmobility.productivity.cosumer.Cart;
import com.bloomsmobility.productivity.cosumer.CartHelper;
import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.Saleable;
import com.bloomsmobility.productivity.cosumer.models.CartItems;
import com.bloomsmobility.productivity.cosumer.models.ProductData;
import com.bloomsmobility.productivity.cosumer.models.Products;
import com.bloomsmobility.productivity.cosumer.utils.Constants;
import com.bloomsmobility.productivity.cosumer.utils.PrefManager;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.bloomsmobility.productivity.cosumer.R.id.search;
import static com.google.android.gms.internal.zzs.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetails extends BaseFragment implements View.OnClickListener {

    private TextView text_pname, text_description, text_quantity, text_price;
    private ImageView image_product;
    private View rootView;
    private ArrayList<Products> productList = new ArrayList<>();
    private Products products;
    static String pro_name;
    private Spinner spinner_qty;
    private Button button_add_to_cart,button_buy;
    int quant;
    PrefManager prefManager;
    int total, prefValue;
    private ProductData productData;

    public ProductDetails() {
        // Required empty public constructor
    }

    public static ProductDetails newInstance(String name) {
        ProductDetails fragment = new ProductDetails();
        pro_name = name;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_product_details, container, false);

        prefManager = new PrefManager(getActivity());
        productData = ProductData.getInstance();

        final Cart cart = CartHelper.getCart();

        text_pname = (TextView) rootView.findViewById(R.id.text_pname);
        text_description = (TextView) rootView.findViewById(R.id.text_description);
        text_price = (TextView) rootView.findViewById(R.id.text_price);
        text_quantity = (TextView) rootView.findViewById(R.id.text_quantity);
        image_product = (ImageView) rootView.findViewById(R.id.image_product);
        button_add_to_cart = (Button) rootView.findViewById(R.id.button_add_to_cart);
        button_add_to_cart.setOnClickListener(this);
        button_buy = (Button) rootView.findViewById(R.id.button_buy);
        button_buy.setOnClickListener(this);
        spinner_qty = (Spinner) rootView.findViewById(R.id.spinner_qty);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(getActivity(), android.R.layout.simple_list_item_1, Constants.QUANTITY_LIST);
        spinner_qty.setAdapter(adapter);

        spinner_qty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(
                    AdapterView<?> adapterView, View view, int i, long l) {
                quant = Integer.parseInt(spinner_qty.getItemAtPosition(i).toString());
            }

            public void onNothingSelected(
                    AdapterView<?> adapterView) {

            }
        });
        prepareProductData();

        return rootView;
    }

    private void prepareProductData() {

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(Constants.TABLE_PRODUCT);

        Query queryRef = myRef.orderByChild(Constants.COLUMN_PRODUCT_NAME).equalTo(pro_name);
        queryRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChild) {
                products = dataSnapshot.getValue(Products.class);

                text_pname.setText(products.getPname());
                text_description.setText(products.getDescription());
                text_quantity.setText(products.getQuantity());
                text_price.setText(String.valueOf(products.getPrice()));

                productData.setProductName(products.getPname());
                productData.setProductDesc(products.getDescription());
                productData.setSubtotal(products.getQuantity());
                productData.setPrice(String.valueOf(products.getPrice()));

                productList.add(products);

                Picasso.with(getActivity())
                        .load(products.getUrl())
                        .into(image_product);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.button_add_to_cart:

                Cart cart = CartHelper.getCart();
                cart.add(products, Integer.valueOf(quant));
                mListener.onFragmentInteraction(Constants.FRAGMENT_CART, null);

                break;
            case R.id.button_buy:
                ProductData.getInstance().setQty(String.valueOf(quant));

                double price = quant * Double.parseDouble(String.valueOf(products.getPrice()));
                Log.d(TAG, "onClick: " + price);
                ProductData.getInstance().setPrice(String.valueOf(price));
                mListener.onFragmentInteraction(Constants.FRAGMENT_CHECKOUT, null);
                break;

        }
    }
}
