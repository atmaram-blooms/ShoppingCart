package com.bloomsmobility.productivity.cosumer.models;

/**
 * Created by Atmaram on 02-06-2017.
 */

public class ProductData {

    String subtotal;
    String price;
    String total;
    String qty;
    String userAddress;
    String userName;
    String phNo;
    String email;
    String pincode;
    String productName;
    String productDesc;

    private static ProductData productData;

    public static ProductData getInstance() {
        if (productData == null)
            productData = new ProductData();
        return productData;
    }

    public ProductData() {
    }

    public ProductData(String subtotal, String price, String total, String userAddress, String userName, String phNo, String email, String pincode, String productName, String productDesc) {
        this.subtotal = subtotal;
        this.price = price;
        this.total = total;
        this.userAddress = userAddress;
        this.userName = userName;
        this.phNo = phNo;
        this.email = email;
        this.pincode = pincode;
        this.productName = productName;
        this.productDesc = productDesc;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhNo() {
        return phNo;
    }

    public void setPhNo(String phNo) {
        this.phNo = phNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public static ProductData getProductData() {
        return productData;
    }

    public static void setProductData(ProductData productData) {
        ProductData.productData = productData;
    }
}
