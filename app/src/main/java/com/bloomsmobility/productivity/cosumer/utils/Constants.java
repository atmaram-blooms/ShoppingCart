package com.bloomsmobility.productivity.cosumer.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shrishyl on 11/11/2016.
 */
public class Constants {

    public static final String PATH_FIREBASE_STORAGE = "gs://tracker-1fdf9.appspot.com";
    public static final String PATH_FIREBASE_DATABASE = "Consumer";
    public static final String PATH_BASE = "/Consumer/";

    public static final String FIREBASE_USER = "Firebase User";

    public static final String TABLE_USER = "USERS";
    public static final String COLUMN_USER_NAME = "name";
    public static final String COLUMN_USER_CONTACT = "contact";
    public static final String COLUMN_USER_EMAIL_ID = "email";
    public static final String COLUMN_USER_ADDRESS = "address";
    public static final String COLUMN_USER_PINCODE = "pincode";
    public static final String COLUMN_USER_PASSWORD = "password";

    public static final String TABLE_PRODUCT = "PRODUCTS";
    public static final String COLUMN_PRODUCT_NAME = "pname";
    public static final String COLUMN_PRODUCT_PRICE = "price";
    public static final String COLUMN_PRODUCT_QUANTITY = "quantity";
    public static final String COLUMN_PRODUCT_DESCRIPTION = "description";
    public static final String COLUMN_PRODUCT_UNIT = "unit";
    public static final String COLUMN_PRODUCT_BAND = "bname";

    public static final String TABLE_CART = "CART";
    public static String COLUMN_CART_PRODUCT_NAME = "cpname";
    public static String COLUMN_CART_PRODUCT_QUANTITY = "cqty";
    public static String COLUMN_CART_PRODUCT_DESCRIPTION = "cdesc";
    public static String COLUMN_CART_PRODUCT_PRICE = "cprice";
    public static String COLUMN_CART_PRODUCT_ADDRESS = "caddress";
    public static String COLUMN_CART_PRODUCT_CONTACT = "ccontact";
    public static String COLUMN_CART_PRODUCT_PINCODE = "cpincode";
    public static String COLUMN_CART_PRODUCT_URL = "curl";
    public static String COLUMN_CART_PRODUCT_ID = "cid";

    public static final int FRAGMENT_PRODUCT = 1;
    public static final int FRAGMENT_PRODUCT_DETAILS = 2;
    public static final int FRAGMENT_CART = 3;
    public static final int FRAGMENT_CHECKOUT = 4;

    public static final List<Integer> QUANTITY_LIST = new ArrayList<Integer>();

    static {
        for (int i = 1; i < 11; i++) QUANTITY_LIST.add(i);
    }

    public static final String CURRENCY = "Rs.";
}
