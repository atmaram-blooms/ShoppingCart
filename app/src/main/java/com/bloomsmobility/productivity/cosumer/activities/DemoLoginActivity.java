package com.bloomsmobility.productivity.cosumer.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.models.UserDetails;
import com.bloomsmobility.productivity.cosumer.utils.Constants;
import com.bloomsmobility.productivity.cosumer.utils.DialogUtil;
import com.bloomsmobility.productivity.cosumer.utils.PrefManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DemoLoginActivity extends AppCompatActivity {

    private static final String TAG = "DemoLoginActivity";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private Context context;

    private ProgressDialog mProgress;

    private EditText usernameTextView, passwordTextView;
    private Button loginButton;
    private TextView forgotpasswordTextView, registerTextView;

    private FirebaseUser user;
    private com.google.firebase.database.Query queryRef;
    private ValueEventListener mValueListener;
    boolean restarted = false;
    String email, password;
    private PrefManager prefManager;

    public static String userDisplayName, userDisplayEmail;
    private String name, emailId, slot1;

    public static final int MULTIPLE_PERMISSIONS = 10;
    String[] permissions = new String[]{
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.ACCESS_COARSE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        context = DemoLoginActivity.this;
        prefManager = new PrefManager(context);

        if (Build.VERSION.SDK_INT < 23) {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            slot1 = tm.getDeviceId();
        }
        if (checkPermissions()) {
        }

        mProgress = new ProgressDialog(context);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    name = user.getDisplayName();
                    emailId = user.getEmail();
                    Log.d(" details ", name + "" + emailId);
                    if (!restarted) {
                        DialogUtil.displayProgress(DemoLoginActivity.this);
                        checkForRegistration();
                    } else {
                        restarted = false;
                    }
                } else {
                }
            }
        };

        usernameTextView = (EditText) findViewById(R.id.edit_text_username);
        passwordTextView = (EditText) findViewById(R.id.edit_text_password);
        loginButton = (Button) findViewById(R.id.button_Login);
        forgotpasswordTextView = (TextView) findViewById(R.id.text_forgot_password);
        registerTextView = (TextView) findViewById(R.id.text_register);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                email = usernameTextView.getText().toString().trim();
                password = passwordTextView.getText().toString().trim();

                if (!isValidEmail(usernameTextView.getText().toString().trim())) {
                    usernameTextView.setError(getString(R.string.invalid_email));
                    usernameTextView.requestFocus();
                    showError(usernameTextView);
                } else if (passwordTextView.getText().toString().isEmpty()) {
                    passwordTextView.setError(getString(R.string.invalid_password));
                    passwordTextView.requestFocus();
                    showError(passwordTextView);
                } else {
                    DialogUtil.displayProgress(DemoLoginActivity.this);
                    mAuth.signInWithEmailAndPassword(usernameTextView.getText().toString(), passwordTextView.getText().toString())
                            .addOnCompleteListener(DemoLoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    dismissProgress(mProgress);

                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                                        prefManager.setUsername(email);
                                        Intent intent = new Intent(DemoLoginActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Log.w(TAG, "signInWithEmail:failed", task.getException());
                                        Toast.makeText(context, R.string.login_unsuccess, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    DialogUtil.stopProgressDisplay();
                }
            }
        });

        forgotpasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DemoLoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        registerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DemoLoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(context, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    slot1 = tm.getDeviceId();
//                    setVariables();
                } else {
//                    Toast.makeText(context, R.string.permissions_warning_generic, Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {

//        Toast.makeText(context, "Destroying LoginActivity()", Toast.LENGTH_SHORT).show();
        try {

            DialogUtil.stopProgressDisplay();

        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private void checkForRegistration() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(Constants.TABLE_USER);

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            Log.d("check", getString(R.string.check_for_reg) + user.getEmail());

            userDisplayName = user.getDisplayName();
            userDisplayEmail = user.getEmail();

            queryRef = myRef.orderByChild(Constants.COLUMN_USER_EMAIL_ID).equalTo(user.getEmail());
            mValueListener = queryRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    UserDetails value = dataSnapshot.getValue(UserDetails.class);
                    Log.d(TAG, " value" + value);
                    if (!dataSnapshot.exists()) {
                        Intent intent = new Intent(DemoLoginActivity.this, RegistrationActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        queryRef.removeEventListener(mValueListener);
                    } else {
                        queryRef.removeEventListener(mValueListener);
                        Intent intent = new Intent(DemoLoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                    DialogUtil.stopProgressDisplay();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private void showError(EditText editText) {
        Animation shake = AnimationUtils.loadAnimation(DemoLoginActivity.this, R.anim.shake);
        editText.startAnimation(shake);
    }

    public void showProgress(ProgressDialog pd, String title) {
        pd.setMessage(title);
        pd.show();
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
    }

    public void dismissProgress(ProgressDialog pd) {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
    }

    public final static boolean isValidEmail(CharSequence email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
