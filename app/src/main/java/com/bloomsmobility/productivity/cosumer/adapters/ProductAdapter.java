package com.bloomsmobility.productivity.cosumer.adapters;

/**
 * Created by Atmaram on 05-06-2017.
 */

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bloomsmobility.productivity.cosumer.Cart;
import com.bloomsmobility.productivity.cosumer.CartHelper;
import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.listener.OnFragmentListItemSelectListener;
import com.bloomsmobility.productivity.cosumer.models.CartItems;
import com.bloomsmobility.productivity.cosumer.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.google.android.gms.internal.zzs.TAG;

public class ProductAdapter extends BaseAdapter {

    private List<CartItems> productList;
    Activity context;
    OnFragmentListItemSelectListener mListener;
    private LayoutInflater inflator;
    private boolean showCheckBox;

    public ProductAdapter(List<CartItems> list, LayoutInflater inflater, boolean sCheckbox) {
        productList = list;
        inflator = inflater;
        showCheckBox = sCheckbox;
    }

    public void setListener(OnFragmentListItemSelectListener mListener) {
        this.mListener = mListener;
    }

    public void updateCartItems(List<CartItems> cartItems) {
        this.productList = cartItems;
        notifyDataSetInvalidated();
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewItem item;

        if (convertView == null) {
            convertView = inflator.inflate(R.layout.item, null);
            item = new ViewItem();

            item.productImageView = (ImageView) convertView.findViewById(R.id.ImageViewItem);

            item.productTitle = (TextView) convertView.findViewById(R.id.TextViewItem);
            item.text_qty = (TextView) convertView.findViewById(R.id.text_quant);
            item.text_total = (TextView) convertView.findViewById(R.id.text_total);

            item.productCheckbox = (CheckBox) convertView.findViewById(R.id.CheckBoxSelected);

            convertView.setTag(item);
        } else {
            item = (ViewItem) convertView.getTag();
        }

        final Cart cart = CartHelper.getCart();
        final CartItems cartItem = productList.get(position);

        Log.d(TAG, "getView: " + cartItem.getProduct().getPname());

        Picasso.with(convertView.getContext())
                .load(cartItem.getProduct().getUrl())
                .into(item.productImageView);

        item.productTitle.setText(cartItem.getProduct().getPname());
        item.text_total.setText(Constants.CURRENCY + String.valueOf(cart.getCost(cartItem.getProduct())));
        item.text_qty.setText("Qty : " + cartItem.getCqty());
        item.productTitle.setText(cartItem.getProduct().getPname());


        final View finalConvertView = convertView;
        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onListItemLongClicked(position, cartItem, finalConvertView);
                return true;
            }
        });

        if (!showCheckBox) {
            item.productCheckbox.setVisibility(View.GONE);
        } else {
            if (cartItem.selected == true)
                item.productCheckbox.setChecked(true);
            else
                item.productCheckbox.setChecked(false);
        }

        return convertView;
    }


    private class ViewItem {
        ImageView productImageView;
        TextView productTitle, text_qty, text_total;
        CheckBox productCheckbox;
    }

}