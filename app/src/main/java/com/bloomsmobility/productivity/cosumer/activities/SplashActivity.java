package com.bloomsmobility.productivity.cosumer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.utils.PrefManager;

public class SplashActivity extends AppCompatActivity {
    private ImageView imageSplash;
    private boolean timerStarted;
    private Animation animator;
    PrefManager prefManager;
    String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setUpXMLVariables();

    }

    public void setUpXMLVariables() {

        imageSplash = (ImageView) findViewById(R.id.imageSplash);
        prefManager = new PrefManager(getApplicationContext());

        value = prefManager.getUsername();

        animator = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        animator.setAnimationListener
                (new Animation.AnimationListener() {
                     @Override
                     public void onAnimationStart(Animation animation) {
                         timerStarted = true;
                     }

                     @Override
                     public void onAnimationEnd(Animation animation) {
                         timerStarted = false;

                         if (value == null) {
                             Intent intent = new Intent(SplashActivity.this, DemoLoginActivity.class);
                             startActivity(intent);
                             finish();
                         } else {
                             Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                             startActivity(intent);
                             finish();
                         }
                     }

                     @Override
                     public void onAnimationRepeat(Animation animation) {
                     }
                 }

                );
        imageSplash.startAnimation(animator);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timerStarted) {
            animator.cancel();
        }
    }
}
