package com.bloomsmobility.productivity.cosumer.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.adapters.ProductAdapter;
import com.bloomsmobility.productivity.cosumer.adapters.UserDetails;
import com.bloomsmobility.productivity.cosumer.listener.OnFragmentListItemSelectListener;
import com.bloomsmobility.productivity.cosumer.models.ProductData;
import com.bloomsmobility.productivity.cosumer.utils.Constants;
import com.bloomsmobility.productivity.cosumer.utils.PrefManager;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import static com.bloomsmobility.productivity.cosumer.fragments.ShoppingCart.clearList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckoutFragment extends BaseFragment implements OnFragmentListItemSelectListener {

    private ProductAdapter mProductAdapter;
    View rootView;
    TextView text_name, text_address, text_total, text_price;
    Button button_order;
    UserDetails userDetails;
    PrefManager prefManager;

    FirebaseDatabase firebaseDatabase;
    static CheckoutFragment fragment;


    public CheckoutFragment() {
        // Required empty public constructor
    }

    public static CheckoutFragment newInstance() {
        if (fragment == null) {
            fragment = new CheckoutFragment();
        }

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_checkout, container, false);

        firebaseDatabase = FirebaseDatabase.getInstance();
        prefManager = new PrefManager(getContext());

        text_name = (TextView) rootView.findViewById(R.id.text_name);
        text_address = (TextView) rootView.findViewById(R.id.text_address);
        text_total = (TextView) rootView.findViewById(R.id.text_subtotal);
        text_price = (TextView) rootView.findViewById(R.id.text_price);
        button_order = (Button) rootView.findViewById(R.id.button_order);

        button_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearList();
                new AlertDialog.Builder(getActivity())
                        .setTitle(getResources().getString(R.string.order_message))
                        .setMessage(getResources().getString(R.string.pop_up))
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mListener.onFragmentInteraction(Constants.FRAGMENT_PRODUCT, null);
                            }
                        })
                        .show();
            }
        });

        getUserDetails();

        return rootView;
    }

    private void getUserDetails() {
       String value = prefManager.getUsername();

        DatabaseReference myRef = firebaseDatabase.getReference(Constants.TABLE_USER);

        Query queryRef = myRef.orderByChild(Constants.COLUMN_USER_EMAIL_ID).equalTo(value);
        queryRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                userDetails = dataSnapshot.getValue(UserDetails.class);

                text_address.setText(userDetails.getAddress());
                text_name.setText(userDetails.getName());
                text_total.setText(ProductData.getInstance().getQty());
                text_price.setText("₹ " + ProductData.getInstance().getPrice());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onListItemSelected(int itemId, Object data) {

    }

    @Override
    public void onListItemLongClicked(int itemId, Object data, View view) {

    }

    public void closeApp() {
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this.getActivity());
        alertDialogBuilder.setMessage(getString(R.string.quit_message));

        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                getActivity().finish();

            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
