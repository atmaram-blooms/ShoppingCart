package com.bloomsmobility.productivity.cosumer.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bloomsmobility.productivity.cosumer.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordActivity extends AppCompatActivity {

    EditText mEmail;
    Button mResetPassword;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_reset);

        mProgress = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();

        mEmail = (EditText) findViewById(R.id.edit_text_email);
        mResetPassword = (Button) findViewById(R.id.button_Reset);


        mResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = mEmail.getText().toString().trim();
                boolean email_check = isValidEmail(email);

                if (!email_check) {
                    mEmail.setError("Invalid Email Address");
                    return;
                }
                if (TextUtils.isEmpty(email)) {
                    mEmail.setError("Email Address is Empty");
//                    Toast.makeText(getActivity(), "Enter email address", Toast.LENGTH_SHORT).show();
                    return;
                }

                mProgress.setMessage("Sending Reset Email..");
                mProgress.show();
                mProgress.setCancelable(false);
                mProgress.setCanceledOnTouchOutside(false);

                mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {

                        mProgress.dismiss();

                        if (task.isSuccessful()) {
                            Toast.makeText(ForgotPasswordActivity.this, "We have sent instructions email to reset your password", Toast.LENGTH_SHORT).show();
//                            mListener.onFragmentInteraction(Constants.FRAGMENT_LOGIN, null);
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, "Failed to send reset email", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });


    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public final static boolean isValidEmail(CharSequence email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
