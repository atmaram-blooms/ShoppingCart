package com.bloomsmobility.productivity.cosumer.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bloomsmobility.productivity.cosumer.R;
import com.bloomsmobility.productivity.cosumer.adapters.ProductsRecyclerView;
import com.bloomsmobility.productivity.cosumer.listener.OnFragmentListItemSelectListener;
import com.bloomsmobility.productivity.cosumer.models.Products;
import com.bloomsmobility.productivity.cosumer.utils.Constants;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ProductFragment extends BaseFragment implements OnFragmentListItemSelectListener {


    private Context context;
    private View rootView;
    private RecyclerView recyclerView;
    private static ProductsRecyclerView productsRecyclerAdapter;
    private static List<Products> productList = new ArrayList<>();
    private static Products products;
    public EditText search;
    static FirebaseDatabase database;

    RecyclerView.LayoutManager mLayoutManager;
    private static List<Products> productItems;

    public ProductFragment() {
        // Required empty public constructor
    }

    public static ProductFragment newInstance() {
        ProductFragment fragment = new ProductFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_product, container, false);
        search = (EditText) rootView.findViewById(R.id.search);

        database = FirebaseDatabase.getInstance();

        if (productList.size() == 0) {
            displayProgress();
            productList = getproductItems(getResources());
        }

        context = ProductFragment.this.getActivity();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.product_list);

        productsRecyclerAdapter = new ProductsRecyclerView(getActivity(), productList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(productsRecyclerAdapter);

        productsRecyclerAdapter.setListener(this);

//        productList.add(products);
//        prepareProductData();

        addTextListener();
        return rootView;
    }


    public void addTextListener() {

        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                updateList(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
            }
        });
    }

    public void updateList(String s) {
        List<Products> resultant = new ArrayList<>();
        for (Products p : productList) {
            if (p.getPname().contains(s)) {
                resultant.add(p);
            }

//            productsRecyclerAdapter = new ProductsRecyclerView(getActivity(), resultant);
            productsRecyclerAdapter.updateCartItems(resultant);
            recyclerView.setAdapter(productsRecyclerAdapter);
//            productsRecyclerAdapter.notifyDataSetChanged();
        }
    }

   /* private void prepareProductData() {

        productList.clear();

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(Constants.TABLE_PRODUCT);

        Query queryRef = myRef.orderByChild(Constants.COLUMN_PRODUCT_NAME);
        queryRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChild) {

                products = dataSnapshot.getValue(Products.class);

                productList.add(products);

                productsRecyclerAdapter.notifyDataSetChanged();
                dismissProgress();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    ProgressDialog progressDialog;

    private void displayProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading));
        }
        progressDialog.show();
    }

    private void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onListItemSelected(int itemId, Object data) {

        search.getText().clear();
        final Products deviceDetails = (Products) data;

        pname = deviceDetails.getPname();
        mListener.onFragmentInteraction(Constants.FRAGMENT_PRODUCT_DETAILS, pname);

    }

    @Override
    public void onListItemLongClicked(int itemId, Object data, View view) {

    }*/

    @Override
    public void onListItemSelected(int itemId, Object data) {
        search.getText().clear();
        final Products deviceDetails = (Products) data;

        String pname = deviceDetails.getPname();
        mListener.onFragmentInteraction(Constants.FRAGMENT_PRODUCT_DETAILS, pname);
    }

    @Override
    public void onListItemLongClicked(int itemId, Object data, View view) {

    }

    public static List<Products> getproductItems(Resources res) {
        if (productItems == null) {
            productItems = new Vector<Products>();

            productList.clear();

            DatabaseReference myRef = database.getReference(Constants.TABLE_PRODUCT);

            Query queryRef = myRef.orderByChild(Constants.COLUMN_PRODUCT_NAME);
            queryRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String previousChild) {
                    products = dataSnapshot.getValue(Products.class);
                    productList.add(products);
                    productsRecyclerAdapter.notifyDataSetChanged();
                    dismissProgress();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });
        }
        return productItems;
    }


    public static ProgressDialog progressDialog;

    private void displayProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.loading));
        }
        progressDialog.show();
    }

    private static void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

}
